#!/usr/bin/env python3

import datetime
import json
import os
import random
import subprocess
import sys
import zoneinfo


def convert_timestamp(input):
  res = datetime.datetime.strptime(input + "+00:00", "%Y%m%dT%H%M%SZ%z")
  return res.astimezone()


def random_clamp(v, m, x):
  if v < m or v > x:
    return random.randint(m, x)
  return v


def is_active(task, active_memo):
  def compute(task, active_memo):
    if task["status"] in ("deleted", "completed", "recurring"):
      return False
    if "depends" in task:
      dom = f"{task['uuid']}.tags.BLOCKED"
      blocker = subprocess.check_output(["task", "_get", dom])
      return blocker.strip() != b"BLOCKED"
    else:
      return True
  if task["uuid"] not in active_memo:
    active_memo[task["uuid"]] = compute(task, active_memo)
  return active_memo[task["uuid"]]


class Xpad:
  running = True

  @staticmethod
  def stop():
    if Xpad.running:
      os.system("xpad -q")
    Xpad.running = False

  @staticmethod
  def start():
    if not Xpad.running:
      os.system("systemctl --user start xpad")
    Xpad.running = True


class Note:

  folder = os.path.expanduser("~/.config/xpad")
  weekday_limits = ((1, 7), (161, 167), (321, 327), (481, 487), (641, 647),
    (801, 807), (961, 967))
  weekday_vlimits = ((104, 121), (244, 261), (384, 401), (524, 541),
    (664, 681), (804, 821), (944, 961))
  week_limits = ((1121, 1127), (1281, 1287), (1441, 1447), (1601, 1607))
  delay_colors = ("rgb(249,240,107)", "rgb(248,228,92)", "rgb(246,211,45)",
    "rgb(245,194,17)", "rgb(229,165,10)", "rgb(255,120,0)", "rgb(230,97,0)",
    "rgb(198,70,0)", "rgb(224,27,36)")

  def __init__(self, id):
    self.id = id
    self.content = ""
    self.due = None
    self.x = 1764
    self.y = 110
    self.color = "rgb(255, 238, 153)"
    self.dirty = False
    self.was_dirty = False

  def set_content(self, new):
    if new != self.content:
      self.content = new
      self._make_dirty()

  def set_due(self, new):
    if new != self.due:
      self.due = new
      self._make_dirty()
      self.constrain_position()
      self.adapt_color()

  def adapt_color(self):
    old_color = self.color

    if self.due is not None:
      now = datetime.datetime.now().astimezone().replace(hour=0, minute=0,
        second=0, microsecond=0)
      if now > self.due:
        day_diff = max(1, (now - self.due).days)
        if day_diff - 1 >= len(Note.delay_colors):
          day_diff = len(Note.delay_colors)
        self.color = Note.delay_colors[day_diff - 1]
      else:
        self.color = "rgb(255, 238, 153)"

    if self.color != old_color:
      self._make_dirty()

  def constrain_position(self):
    old_x = self.x
    old_y = self.y

    self.y = random_clamp(self.y, 104, 961)
    if self.due is None:
      self.x = random_clamp(self.x, 1759, 1767)
    else:
      now = datetime.datetime.now().astimezone().replace(hour=0, minute=0,
        second=0, microsecond=0)
      due = self.due
      if now > due:
        due = now
      day_diff = (due - now).days
      target_weekday = (now.weekday() + day_diff) % 7
      if day_diff < 7:
        limits = Note.weekday_limits[target_weekday]
        self.x = random_clamp(self.x, limits[0], limits[1])
      elif day_diff < 35:
        week = (day_diff - 7) // 7
        limits_x = Note.week_limits[week]
        limits_y = Note.weekday_vlimits[target_weekday]
        self.x = random_clamp(self.x, limits_x[0], limits_x[1])
        self.y = random_clamp(self.y, limits_y[0], limits_y[1])
      else:
        self.x = random_clamp(self.x, 1759, 1767)

    if self.x != old_x or self.y != old_y:
      self._make_dirty()

  def write(self):
    if not self.dirty:
      return
    self.dirty = False

    with open(Note.folder + f"/info-{self.id}", "w") as file:
      file.write(self._info_content())
    with open(Note.folder + f"/meta-{self.id}", "w") as file:
      file.write(self._meta_content())
    with open(Note.folder + f"/content-{self.id}", "w") as file:
      file.write(self.content)

  def delete(self):
    for what in (f"/info-{self.id}", f"/content-{self.id}", f"/meta-{self.id}"):
      try:
        if os.path.exists(Note.folder + what):
          self._make_dirty() # need to potentially stop xpad before removing
        os.remove(Note.folder + what)
      except FileNotFoundError:
        pass

  @staticmethod
  def load(id):
    note = Note(id)
    try:
      with open(Note.folder + f"/info-{id}", "r") as info:
        with open(Note.folder + f"/meta-{id}", "r") as meta:
          with open(Note.folder + f"/content-{id}", "r") as content:
            note.content = content.read()
          for line in info:
            data = line.strip().split(" ", 1)
            if data[0] == "x":
              note.x = int(data[1])
            if data[0] == "y":
              note.y = int(data[1])
            if data[0] == "back":
              note.color = data[1]
          for line in meta:
            data = line.strip().split(" ", 1)
            if data[0] == "due":
              note.due = datetime.datetime.strptime(data[1], "%Y%m%dT%H%M%SZ%z")
    except FileNotFoundError:
      note._make_dirty()
    note.constrain_position()
    note.adapt_color()
    return note

  def _make_dirty(self):
    self.dirty = True
    if not self.was_dirty:
      Xpad.stop()
    self.was_dirty = True

  def _meta_content(self):
    res = ""
    if self.due is not None:
      res += f"due {self.due.strftime('%Y%m%dT%H%M%SZ%z')}\n"
    return res

  def _info_content(self):
    res = ""
    res += "width 152\n"
    res += "height 118\n"
    res += f"x {self.x}\n"
    res += f"y {self.y}\n"
    res += "follow_font 1\n"
    res += "follow_color 0\n"
    res += "sticky 1\n"
    res += "hidden 0\n"
    res += f"back {self.color}\n"
    res += "text rgb(0,0,0)\n"
    res += "fontname Fira Sans 12\n"
    res += f"content content-{self.id}\n"
    return res

tasks = []
while True:
  task = sys.stdin.readline()
  if task == "":
    break
  tasks.append(json.loads(task))

# Uncomment for debugging - shows input by taskwarrior before anything is done
# print(json.dumps(tasks))
# sys.exit(0)  # uncomment this for disabling any and all changes


active_memo = {}
for task in tasks:
  note = Note.load(task["uuid"])
  if not is_active(task, active_memo):
    note.delete()
  else:
    note.set_content(task["description"])
    if "due" in task:
      note.set_due(convert_timestamp(task["due"]))
    else:
      note.set_due(None)
    note.write()

# TODO: requirements, install instructions
# TODO: taskwarrior aliases: touch all, gc

Xpad.start()
sys.exit(0)
